//
//  CircleRecord.swift
//  EyePhoto
//
//  Created by O1-Developer on 2020/1/17.
//  Copyright © 2020 O1-Developer. All rights reserved.
//
import UIKit
import Foundation

struct CircleRecord {
    var radiusArray: [CGFloat]
    var centerArray: [CGPoint]
    var image: UIImage
    var originLength: CGFloat
    var id: Int
}
