//
//  ViewExtension.swift
//  IrisAnalysis
//
//  Created by Yo on 2020/4/4.
//  Copyright © 2020 Yo. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

// MARK: - Crop
extension UIView {
    func cropRect(rect: CGRect) -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: rect)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
    func cropLeftHalf() -> UIImage {
        self.cropRect(rect: CGRect(x: 0, y: 0, width: self.bounds.width * 0.5, height: self.bounds.height))
    }
    
    func cropRightHalf() -> UIImage {
        self.cropRect(rect: CGRect(x: self.bounds.width * 0.5, y: 0, width: self.bounds.width * 0.5, height: self.bounds.height))
    }
}

// MARK: - ViewControl
private var viewTapAssociate: UInt8 = 1
extension UIView {
    @objc func handlePan(gesture: UIPanGestureRecognizer){
        if gesture.state == .changed {
            var center = gesture.view?.center
            let translation = gesture.translation(in: gesture.view)
            //放大縮小後 移動
            let scale = gesture.view?.transform.a
            center = CGPoint(x: center!.x + (translation.x * scale!), y: center!.y + (translation.y * scale!))
            gesture.view?.center = center!
            gesture.setTranslation(CGPoint.zero, in: gesture.view)
        }
    }
    @objc func handlePinch(gesture: UIPinchGestureRecognizer){
        switch gesture.state {
            case .changed:
//                if let view = gesture.view as? UIImageView {
//                    print(gesture.scale)
                    self.transform = self.transform.scaledBy(x: gesture.scale, y: gesture.scale)
                    gesture.scale = 1
//                }
                
            default:
                break
        }
    }
    @objc func handleTap(gesture: UITapGestureRecognizer) {
        if let tap = tapBlock {
            tap(gesture)
        }
    }
    
    var tapBlock: ((_ gesture: UITapGestureRecognizer) -> ())? {
        get { return objc_getAssociatedObject(self, &viewTapAssociate) as? (_ gesture: UITapGestureRecognizer) -> ()  }
        set { objc_setAssociatedObject(self, &viewTapAssociate, newValue, .OBJC_ASSOCIATION_RETAIN) }
    }
}
