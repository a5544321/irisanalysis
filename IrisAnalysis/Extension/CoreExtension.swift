//
//  CoreExtension.swift
//  IrisAnalysis
//
//  Created by Yo on 2020/4/4.
//  Copyright © 2020 Yo. All rights reserved.
//
import UIKit
import Foundation
private var shapeRadiusKey: UInt8 = 1
private var shapeCenterKey: UInt8 = 2

extension CAShapeLayer{
    var radius: CGFloat {
        get { return objc_getAssociatedObject(self, &shapeRadiusKey) as? CGFloat ?? 50 }
        set { objc_setAssociatedObject(self, &shapeRadiusKey, newValue, .OBJC_ASSOCIATION_RETAIN) }
    }
    var center: CGPoint {
        get { return objc_getAssociatedObject(self, &shapeCenterKey) as? CGPoint ?? CGPoint(x: 10, y: 10) }
        set { objc_setAssociatedObject(self, &shapeCenterKey, newValue, .OBJC_ASSOCIATION_RETAIN) }
    }
}

extension CGPoint {
    static func + (left: CGPoint, right: CGPoint) -> CGPoint {
      return CGPoint(x: left.x + right.x, y: left.y + right.y)
    }

    static func * (left: CGPoint, right: CGFloat) -> CGPoint {
        return CGPoint(x: left.x * right, y: left.y * right)
    }
}

protocol Number {
    var floatValue: Float {get}
    var doubleValue: Double {get}
    var intValue: Int {get}
    var cgFloatValue: CGFloat {get}
    
}
extension Number where Self: BinaryInteger {
    var floatValue: Float {
        return Float(self)
    }
    var doubleValue: Double {
        return Double(self)
    }
    var intValue: Int {
        return Int(self)
    }
    var cgFloatValue: CGFloat {
        return CGFloat(self)
    }

}

extension Int: Number {}
extension Float: Number {
    var floatValue: Float {
        return Float(self)
    }
    var doubleValue: Double {
        return Double(self)
    }
    var intValue: Int {
        return Int(self)
    }
    var cgFloatValue: CGFloat {
        return CGFloat(self)
    }
}
extension Double: Number {
    var floatValue: Float {
        return Float(self)
    }
    var doubleValue: Double {
        return Double(self)
    }
    var intValue: Int {
        return Int(self)
    }
    var cgFloatValue: CGFloat {
        return CGFloat(self)
    }
}
extension CGFloat: Number {
    var floatValue: Float {
        return Float(self)
    }
    var doubleValue: Double {
        return Double(self)
    }
    var intValue: Int {
        return Int(self)
    }
    var cgFloatValue: CGFloat {
        return CGFloat(self)
    }
}
