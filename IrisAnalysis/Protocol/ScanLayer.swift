//
//  ScanLayer.swift
//  IrisAnalysis
//
//  Created by Yo on 2020/4/4.
//  Copyright © 2020 Yo. All rights reserved.
//

import Foundation
import UIKit

protocol ScanLayerDelegate: AnyObject {
    func selectLayer(circleView: CircleView, layer: CAShapeLayer)
    func layersReadyToDefine(layers: [CAShapeLayer])
    func circleLayerGenerate(addCircleView: CircleView, layers: [CAShapeLayer])
}
