//
//  CircleView.swift
//  IrisAnalysis
//
//  Created by Yo on 2020/4/4.
//  Copyright © 2020 Yo. All rights reserved.
//

import Foundation
import UIKit

protocol CircleView: UIView {
    var maxLength: CGFloat { get }
}

extension CircleView {
    var maxLength: CGFloat {
        return min(self.bounds.width, self.bounds.height)
    }
}
