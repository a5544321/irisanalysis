//
//  ViewControl.swift
//  IrisAnalysis
//
//  Created by Yo on 2020/4/4.
//  Copyright © 2020 Yo. All rights reserved.
//
import UIKit
import Foundation

protocol MoveableView {
    func addMoveGesture()
}
protocol ScaleableView {
    func addPinchGesture()
}
protocol TapableView {
    func addTapGesture(handler: @escaping (_ gesture: UITapGestureRecognizer) -> ())
}

extension TapableView where Self: UIView{
    
    func addTapGesture(handler: @escaping (_ gesture: UITapGestureRecognizer) -> ()){
        let tg = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        self.addGestureRecognizer(tg)
        self.isUserInteractionEnabled = true
        self.tapBlock = handler
    }
    
}

extension MoveableView where Self: UIView{
    func addMoveGesture(){
        let pg = UIPanGestureRecognizer(target: self, action: #selector(handlePan(gesture:)))
        self.addGestureRecognizer(pg)
        self.isUserInteractionEnabled = true
    }
    
}

extension ScaleableView where Self: UIView{
    
    func addPinchGesture(){
        let pg = UIPinchGestureRecognizer(target: self,
                                          action: #selector(handlePinch(gesture:)))
        self.addGestureRecognizer(pg)
        self.isUserInteractionEnabled = true
    }
    
}

extension UIView: MoveableView, ScaleableView, TapableView {}
