//
//  TwoCircleView.swift
//  EyePhoto
//
//  Created by O1-Developer on 2019/10/17.
//  Copyright © 2019 O1-Developer. All rights reserved.
//

import UIKit


enum CircleDivideType {
    case circle, block
}

class AddCircleView: UIView, CircleView {
    var layerBigCir: CAShapeLayer?
    var layerSmallCir: CAShapeLayer?
    var layerEditing: CAShapeLayer?
    var state: EditState = .edit
    var divideType: CircleDivideType = .block
    weak var delegate: ScanLayerDelegate?
    var finalStrokeColor = UIColor.blue
    
    enum EditState {
        case edit
        case final
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        if frame.width > 0 {
            addNewCircleLayer(radius: maxLength / 2 - 1)
            addPinchGesture()
            addMoveGesture()
        }
        // Center in Parent and make self square
        let length = maxLength
        self.frame = CGRect(origin: CGPoint(x: (frame.width - length) * 0.5, y: (frame.height - length) * 0.5), size: CGSize(width: length, height: length))
    }
    
    convenience init(frame: CGRect, record: CircleRecord, divideType: CircleDivideType) {
//        self.init(frame: CGRect.zero)
        self.init()
        // Center in Parent and make self square
        let length = min(frame.width, frame.height)
        self.frame = CGRect(origin: CGPoint(x: (frame.width - length) * 0.5, y: (frame.height - length) * 0.5), size: CGSize(width: length, height: length))
        self.divideType = divideType
        finalStrokeColor = UIColor.red
//        backgroundColor = UIColor.init(argb: 0x44438621)
        addCirclesByRecord(record: record)
    }
    
    private func addCirclesByRecord(record: CircleRecord) {
        print(min(self.bounds.width, self.bounds.height))
        let percentChange = min(self.bounds.width, self.bounds.height) / record.originLength
        // 改變圓圈半徑和中心
        let newRadiusArr = record.radiusArray.map{ $0 * percentChange}
        let newCenterArr = record.centerArray.map { $0 * percentChange}
        
        for i in newRadiusArr.indices {
            addNewCircleLayer(radius: newRadiusArr[i], center: newCenterArr[i])
        }
        nextStep()
    }
    
    func addNewCircleLayer( radius: CGFloat = 0, center: CGPoint = CGPoint.zero){
        var radius = radius
        endEditLayer()
       
        let mLayer = CAShapeLayer()
        mLayer.fillColor = UIColor.clear.cgColor
        mLayer.strokeColor = UIColor.green.cgColor
        if radius == 0 { radius = maxLength / 4}
        
        if center == CGPoint.zero {
            mLayer.center = CGPoint(x: maxLength / 2, y: maxLength / 2)
        }else {
            mLayer.center = center
        }
        print(maxLength)
        mLayer.radius = radius
        
        mLayer.lineWidth = 2
        mLayer.path = getCirclePath(center:mLayer.center ,radius: mLayer.radius).cgPath
        layerEditing = mLayer
        self.layer.insertSublayer(mLayer, at: UInt32( self.layer.sublayers?.count ?? 0) )
        
//        print(self.layer.sublayers?.count)
    }
    
    func removeLastCircle() {
        guard (self.layer.sublayers?.count ?? 0) > 2 else {
            return
        }
        self.layer.sublayers?.remove(at: self.layer.sublayers!.endIndex - 2)
    }
    
    func endEditLayer(){
        layerEditing?.strokeColor = UIColor.red.cgColor
        layerEditing = nil
    }
    
    private func getCirclePath(center: CGPoint, radius: CGFloat) -> UIBezierPath{
        
        let path = UIBezierPath()
        path.addArc(withCenter: center, radius: radius, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        return path
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

       if state == .final{
            if let touch = touches.first {
                let location = touch.location(in: self)
                guard let sublayers = self.layer.sublayers as? [CAShapeLayer] else { return }

                for layer in sublayers {
                    if let path = layer.path, path.contains(location) {
//                        layer.fillColor = UIColor(red: 0.8, green: 0.8, blue: 0, alpha: 0.2).cgColor
                        layer.strokeColor = UIColor.red.cgColor
                        layer.zPosition = 1
                        print(layer.name)
                        delegate?.selectLayer(circleView: self, layer: layer)
                    }else{
                        layer.fillColor = UIColor.clear.cgColor
                        layer.strokeColor = UIColor.blue.cgColor
                        layer.zPosition = 0
                    }
                }
            }
        }
    }
    
    
    func addMiddleLayers(count: Int) {
        guard self.layer.sublayers?.count == 2 else {
            return
        }
        guard let sublayers = self.layer.sublayers as? [CAShapeLayer], sublayers.count > 1 else { return }
        let sortedLayers = sublayers.sorted { (l, ll) -> Bool in
            print("s \(l.radius)  b \(ll.radius)")
           return l.radius < ll.radius
        }
        let small = sortedLayers[0]
        let big = sortedLayers[1]
        
        let distance = hypot(big.center.x - small.center.x, big.center.y - small.center.y)
        let minDiff = big.radius - small.radius - distance
        let unit = minDiff / (count + 1).cgFloatValue
        for i in 1...count {
            addNewCircleLayer(radius: small.radius + unit * i.cgFloatValue, center: small.center)
        }
    }
    
    func nextStep(){
        guard state != .final,
            let sublayers = self.layer.sublayers,
            sublayers.count > 1 else {
            return
        }
        state = .final
        endEditLayer()
        addDivideLayer()
    }
    
    private func addDivideLayer(){
        let center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        guard let sublayers = self.layer.sublayers as? [CAShapeLayer], sublayers.count > 1 else { return }
        let sortedLayers = sublayers.sorted { (l, ll) -> Bool in
//            print("s \(l.radius)  b \(ll.radius)")
           return l.radius < ll.radius
        }
        self.delegate?.circleLayerGenerate(addCircleView: self, layers: sublayers)
        self.layer.sublayers?.removeAll()
        
        if divideType == .circle
        {
            for index in 1..<sortedLayers.count {
                
                let sLayer = sortedLayers[index - 1]
                let bLayer = sortedLayers[index]
                
                let layer = CAShapeLayer()
                layer.fillColor = UIColor.clear.cgColor
                layer.strokeColor = UIColor.blue.cgColor
                let path = UIBezierPath()
                let cutPath = UIBezierPath()
                path.addArc(withCenter: bLayer.center, radius: bLayer.radius, startAngle:0, endAngle: 2 * .pi, clockwise: true)
                cutPath.addArc(withCenter: sLayer.center, radius: sLayer.radius, startAngle:0, endAngle: 2 * .pi, clockwise: true)

                path.append(cutPath.reversing())
                
                layer.path = path.cgPath
                
                let ani = CABasicAnimation(keyPath: "strokeEnd")
                ani.duration = 2.0
                ani.fromValue = 0
                ani.toValue = 1
                layer.add(ani, forKey: "")
                
                self.layer.addSublayer(layer)
               
            }
            
        }
        else if divideType == .block
        {
            let perAngle = -30 * Double.pi / 180
            for i in 0..<12 {
                for index in 1..<sortedLayers.count {
                    let startAngle = perAngle * Double(i) + -75 * Double.pi / 180  // 從最上方開始
                    let finishAngle = startAngle + perAngle
                    let bLayer = sortedLayers[index - 1]
                    let sLayer = sortedLayers[index]
                    
                    let layer = CAShapeLayer()
                    layer.fillColor = UIColor.clear.cgColor
                    layer.strokeColor = finalStrokeColor.cgColor
                    let path = UIBezierPath()
                    path.addArc(withCenter: bLayer.center, radius: bLayer.radius, startAngle:CGFloat(startAngle), endAngle: CGFloat(finishAngle), clockwise: false)
                    path.addLine(to: CGPoint(x: sLayer.center.x + CGFloat(cos(finishAngle)) * CGFloat(sLayer.radius), y: sLayer.center.y + CGFloat(sin(finishAngle)) * CGFloat(sLayer.radius)))
                    path.addArc(withCenter: sLayer.center, radius: sLayer.radius, startAngle:CGFloat(finishAngle), endAngle: CGFloat(startAngle), clockwise: true)
                    path.close()
                    
                    layer.path = path.cgPath
//                    layer.frame = path.cgPath.boundingBoxOfPath
                    layer.name = String(format: "%d-%d", 12 - i, index)
//                    print(path.cgPath.boundingBoxOfPath)
                    
                    let textLayer = CATextLayer()
                    textLayer.alignmentMode = .center
                    textLayer.fontSize = 8
                    textLayer.foregroundColor = UIColor.yellow.cgColor
                    textLayer.contentsScale = UIScreen.main.scale
                    // Dynamic
                    let rect = CGRect(x: path.cgPath.boundingBoxOfPath.midX - 12, y: path.cgPath.boundingBoxOfPath.midY - 6, width: 25, height: 12)
                    print(rect)
                    textLayer.frame = rect
                    textLayer.string = layer.name
                    layer.addSublayer(textLayer)
                    
                    self.layer.addSublayer(layer)
                    
                    let ani = CABasicAnimation(keyPath: "strokeEnd")
                    ani.duration = 1.0
                    ani.fromValue = 0
                    ani.toValue = 1
                    layer.add(ani, forKey: "")
                   
                }
               
            }
            self.delegate?.layersReadyToDefine(layers: self.layer.sublayers as! [CAShapeLayer])
        }
        
       
    }
}

// MARK: - Handle user event
extension AddCircleView {
    // 覆寫原本移動，改為移動Layer
    override func handlePan(gesture: UIPanGestureRecognizer) {
        if gesture.state == .changed, let beforeCenter = layerEditing?.center {
            let translation = gesture.translation(in: gesture.view)
            let newCenter = CGPoint(x: beforeCenter.x + translation.x, y: beforeCenter.y + translation.y)
           
            layerEditing?.center = newCenter
            layerEditing?.path = getCirclePath(center: newCenter, radius: layerEditing!.radius).cgPath
            gesture.setTranslation(CGPoint.zero, in: gesture.view)
        }
        if gesture.state == .ended, let beforeCenter = layerEditing?.center {
            // 超出範圍 移回範圍內
            let translation = gesture.translation(in: gesture.view)
            var newCenter = CGPoint(x: beforeCenter.x + translation.x, y: beforeCenter.y + translation.y)
            let minX = newCenter.x - layerEditing!.radius
            let maxX = newCenter.x + layerEditing!.radius
            let minY = newCenter.y - layerEditing!.radius
            let maxY = newCenter.y + layerEditing!.radius
            if minX < 0 {
                newCenter.x += abs(minX)
            }
            if maxX > self.bounds.maxX {
                newCenter.x -= (maxX - self.bounds.maxX)
            }
            if minY < 0 {
                newCenter.y += abs(minY)
            }
            if maxY > self.bounds.maxY {
                newCenter.y -= (maxY - self.bounds.maxY)
            }
            layerEditing?.center = newCenter
            layerEditing?.path = getCirclePath(center: newCenter, radius: layerEditing!.radius).cgPath
        }
        
    }
    
    override func handlePinch(gesture: UIPinchGestureRecognizer) {
        switch gesture.state {
        case .ended:
            layerEditing?.radius = min(self.bounds.width / 2, layerEditing!.radius * gesture.scale)
        case .changed:
            if let layerEditing = layerEditing{
                let newRadius = layerEditing.radius * gesture.scale
                if newRadius >= maxLength / 2 {
                    // 過大直接置中，半徑最大
                    let viewCenter = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
                    layerEditing.path = getCirclePath(center: viewCenter, radius: maxLength / 2).cgPath
                    layerEditing.center = viewCenter
                }else{
                    layerEditing.path = getCirclePath(center: layerEditing.center, radius: newRadius).cgPath
                }
                
            }
            
        default:
            break
        }
    }
}
