//
//  ViewController.swift
//  IrisAnalysis
//
//  Created by Yo on 2020/4/4.
//  Copyright © 2020 Yo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var ivEye: UIImageView!
    @IBOutlet weak var ivBlock: UIImageView!
    @IBOutlet weak var btnPlus: UIButton!
    
    var mCircleView: AddCircleView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ivBlock.addMoveGesture()
        ivBlock.addPinchGesture()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mCircleView = AddCircleView(frame: ivEye.bounds)
        mCircleView?.delegate = self
        ivEye.addSubview(mCircleView!)
    }
    
    @IBAction func onAddBtnPress(_ sender: UIButton) {
        mCircleView?.addNewCircleLayer()
    }
    
    @IBAction func onFinishBtnPress(_ sender: Any) {
        mCircleView?.nextStep()
    }
    
}

extension ViewController: ScanLayerDelegate {
    func selectLayer(circleView: CircleView, layer: CAShapeLayer) {
        // 放大後裁切區塊以免模糊
        let size: CGFloat = 700
        let mask = CAShapeLayer()
        let scale = size / ivEye.bounds.size.width
        var transform: CGAffineTransform = .identity
        transform = transform.scaledBy(x: scale, y: scale)
        mask.path = layer.path?.copy(using: &transform)
        
        let tempIv = UIImageView(image: ivEye.image)
        tempIv.frame = .init(origin: .zero, size: CGSize(width: size, height: size))
        tempIv.layer.mask = mask
        
        let maskImage = tempIv.cropRect(rect: mask.path!.boundingBoxOfPath)
        ivBlock.image = maskImage
    }
    
    func layersReadyToDefine(layers: [CAShapeLayer]) {
        btnPlus.isHidden = true
    }
    
    func circleLayerGenerate(addCircleView: CircleView, layers: [CAShapeLayer]) {
        
    }
    
    
}
